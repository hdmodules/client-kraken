<?php

declare(strict_types=1);

namespace Kraken;

use Kraken\Exceptions\InvalidResponseCode;
use Kraken\Request\OrderBookRequest;
use Kraken\Response\OrderBookResponse;
use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\Serializer;
use GuzzleHttp\Client as GuzzleHttpClient;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Class Client
 *
 * @package hdmodules\client-kraken
 * @author  Test Assignment
 */
class Client
{
    /** @var string */
    private $baseUrl = 'https://api.kraken.com/0';

    /** @var GuzzleHttpClient */
    private $httpClient;

    /** @var Serializer */
    private $serializer;

    /**
     * @param array $httpClientOptions
     */
    public function __construct(array $httpClientOptions = [])
    {
        $this->httpClient = new GuzzleHttpClient($httpClientOptions);

        $this->serializer = SerializerBuilder::create()->build();
    }

    /**
     * @param OrderBookRequest $request
     *
     * @throws GuzzleException|InvalidResponseCode
     * @return OrderBookResponse
     */
    public function getOrderBook(OrderBookRequest $request): OrderBookResponse
    {
        $response = $this->sendPublicRequest('Depth', $request->getQuery());

        $orderBookResponse = $this->serializer->deserialize($response, OrderBookResponse::class, 'json');

        return $orderBookResponse;
    }

    /**
     * @param string $endpoint
     * @param array $query
     *
     * @throws GuzzleException|InvalidResponseCode
     * @return string
     */
    private function sendPublicRequest(string $endpoint, array $query): string
    {
        $url = $this->baseUrl.'/public/'.$endpoint;

        $result = $this->httpClient->request('GET', $url, ['query'=>$query]);

        if ($result->getStatusCode() !== 200) {
            throw new InvalidResponseCode('Inspect Response HTTP Code is not correct');
        }

        $stream   = $result->getBody();
        $contents = $stream->getContents();

        return $contents;
    }

}