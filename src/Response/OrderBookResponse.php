<?php

declare(strict_types=1);

namespace Kraken\Response;

use JMS\Serializer\Annotation as Serializer;

/**
 * Class OrderBookRequest
 *
 * @package hdmodules\client-kraken
 * @author  Test Assignment
 */
class OrderBookResponse
{
    /**
     * @Serializer\Expose()
     * @Serializer\Type("array")
     *
     * @var array
     */
    private $error;

    /**
     * @Serializer\Expose()
     * @Serializer\Type("array")
     *
     * @var array
     */
    private $result;

    /**
     * @return array|null
     */
    public function getError(): ?array
    {
        return $this->error;
    }

    /**
     * @return array|null
     */
    public function getResult(): ?array
    {
        return $this->result;
    }

    /**
     * @return array
     */
    public function getAverageExchangeRate(): array
    {
        $result = [];

        $pairs = $this->getResult();

        if($pairs && !empty($pairs)){

            $alias = array_keys($pairs)[0];

            $asks = $pairs[$alias]['asks'];
            $bids = $pairs[$alias]['bids'];

            $result['asks'] = $this->calculateAverage($asks);
            $result['bids'] = $this->calculateAverage($bids);

        }

        return $result;
    }

    /**
     * @param array $data
     * @return float
     */
    private function calculateAverage(array $data): float
    {
        $sumPrice  = 0;
        $sumVolume = 0;

        foreach ($data as $value){
            $sumPrice  += $value[0]*$value[1];
            $sumVolume += $value[1];
        }

        return $sumVolume ? $sumPrice/$sumVolume : 0;
    }

}