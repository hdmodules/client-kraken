<?php

declare(strict_types=1);

namespace Kraken\Request;

/**
 * Class OrderBookRequest
 *
 * @package hdmodules\client-kraken
 * @author  Test Assignment
 */
class OrderBookRequest
{
    /** @var string */
    public $pair;

    /** @var int */
    public $count;

    /**
     * OrderBookRequest constructor.
     * @param string $pair
     * @param int $count
     */
    public function __construct($pair, $count=100)
    {
        $this->pair  = $pair;
        $this->count = $count;
    }

    /**
     * @return array
     */
    public function getQuery(){

        return get_object_vars($this);
    }
}