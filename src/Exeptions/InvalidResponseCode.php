<?php

declare(strict_types=1);

namespace Kraken\Exceptions;

/**
 * Class InvalidResponseCode
 *
 * @package hdmodules\client-kraken
 * @author  Test Assignment
 */
class InvalidResponseCode extends ErrorException
{
}