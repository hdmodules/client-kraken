<?php

declare(strict_types=1);

namespace Kraken\Exceptions;

/**
 * Class ErrorException
 *
 * @package hdmodules\client-kraken
 * @author  Test Assignment
 */
class ErrorException extends \Exception
{
}