<?php

// Example run command: php average_exchange_rate.php --pair=BTCUSD --count=10

require __DIR__ . '/vendor/autoload.php';

use Kraken\Client;
use Kraken\Request\OrderBookRequest;

$options = getopt("", ["pair:", 'count::']);

if(isset($options['pair'])){

    $pair = $options['pair'];
    $count = 100;

    if(isset($options['count'])){
        $count = (int) $options['count'];
    }

    $client = new Client();

    $orderBookRequest = new OrderBookRequest($pair, $count);
    $orderBookResponse = $client->getOrderBook($orderBookRequest);

    $errors = $orderBookResponse->getError();

    if(empty($errors)){

        $averageExchangeRate = $orderBookResponse->getAverageExchangeRate();

        echo 'Average exchange rate for '.$pair.PHP_EOL;

        foreach ($averageExchangeRate as $k=>$value){
            echo $k.': '.$value.PHP_EOL;
        }

    }else{

        foreach ($errors as $errorMsg){
            echo $errorMsg.PHP_EOL;
        }

    }

}else{

    echo 'Pls set required argument --pair= '.PHP_EOL;

}