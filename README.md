# Test assignment Kraken API integration

Please take a look at the Kraken REST API Documentation, especially at the "Get Order Book" endpoint.

### Specification:
- Create a REST API Client.
- Implement the “Get Order Book” endpoint.
- Create a console/web script to calculate an average exchange rate of a specified
pair and volume (e.g. BTCUSD and volume 1 BTC).

### Requirements:
- Requests and responses must be objects. So use some serializer like JMS or
other

# Usage

```
php average_exchange_rate.php --pair=BTCUSD --count=10

```